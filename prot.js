// Создать конструктор Student для объекта студента, который принимает имя и кол-во баллов в качестве аргумента


function Student (student, point) {
  this.student =  student
  this.point = point
}

Student.prototype.show =  function () {
  console.log("Студент %s набрал %s баллов", this.student, this.point)
}

var student = new Student('Иванов Петр', 20)


// Создать конструктор StudentList, который в качестве аргумента принимает название группы и массив формата studentsAndPoints

var studentsAndPoints = ['Алексей Петров', 0, 'Ирина Овчинникова', 60, 'Глеб Стукалов', 30, 'Антон Павлович', 30, 'Виктория Заровская', 30, 'Алексей Левенец', 70, 'Тимур Вамуш', 30, 'Евгений Прочан', 60, 'Александр Малов', 0]

function StudentList (groupName, groupArray) {
  this.groupName = groupName
  if (groupArray === undefined) {
    groupArray = []
  }
  this.groupArray = groupArray
  this.add = function (student, point) {
    groupArray.push(student, point)
  }
}

// Создать список студентов группы «HJ-2» и сохранить его в переменной hj2. Воспользуйтесь массивом studentsAndPoints
var hj2 = new StudentList('HJ-2', studentsAndPoints)

// Добавить несколько новых студентов в группу

hj2.add('Павел Рыбкин', 90)
hj2.add('Алла Гигова', 0)
hj2.add('Сергей Шопик', 30)

// Создать группу «HTML-7» без студентов и сохранить его в переменной html7. После чего добавить в группу несколько студентов
var html7 = new StudentList('HTML-7')

html7.add('Антон Нигга', 0)
html7.add('Ольга Ше', 20)
html7.add('Николай Антонов', 10)

// Добавить спискам студентов метод show

StudentList.prototype.show = function () {
  var length = this.groupArray.length / 2
  console.log('Группа %s (%s студентов)', this.groupName, length)
  for (var i = 0; i < this.groupArray.length; i += 2) {
      console.log('Студент %s набрал %s баллов', this.groupArray[i], this.groupArray[i+1])
  }
}

// Перевести одного из студентов из группы «HJ-2» в группу «HTML-7»

StudentList.prototype.change = function (array, count) {
    array.groupArray.push(this.groupArray.splice(count*2,1), this.groupArray.splice(count*2, 1))
}

hj2.change(html7,5)
